function getInputNumber(promptText, defaultValue) {
  while (true) {
    const input = prompt(promptText, defaultValue);
    if (input === null) {
      return null;
    }
    const number = parseFloat(input);
    if (!isNaN(number)) {
      return number;
    }
    alert("Будь ласка, введіть коректне число.");
  }
}

function getOperation() {
  while (true) {
    const operation = prompt("Введіть операцію (+, -, *, /)");
    if (operation === "+" || operation === "-" || operation === "*" || operation === "/") {
      return operation;
    }
    alert("Будь ласка, введіть коректну операцію (+, -, *, /).");
  }
}

function performOperation(a, b, operation) {
  switch (operation) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      if (b !== 0) {
        return a / b;
      } else {
        alert("Ділення на нуль неможливе.");
        return null;
      }
  }
}

const number1 = getInputNumber("Введіть перше число:");
if (number1 !== null) {
  const number2 = getInputNumber("Введіть друге число:");
  if (number2 !== null) {
    const operation = getOperation();
    
    const result = performOperation(number1, number2, operation);
    if (result !== null) {
      console.log(`Результат: ${result}`);
    }
  }
}
